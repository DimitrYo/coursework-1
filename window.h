#ifndef WINDOW_H
#define WINDOW_H

#include <QtGui>
#include <QWidget>
#include <QApplication>
class Widget;
class Window : public QWidget
{
    Q_OBJECT
    Widget *widget;
    QLabel *penWidthLabel;
    QLabel *penStyleLabel;
    QLabel *squareLabel;
    QLabel *lengthLabel;
    QLabel *perimetrLabel;
    QLabel *lengthperimLabel;
    QLCDNumber *squareLCD;
    QLCDNumber *lengthLCD;
    QLCDNumber *perimetrLCD;
    QPushButton *startbutton;
    QPushButton *clearscreenbutton;
    QPushButton *squarebutton;
    QPushButton *quitbutton;
    QPushButton *lengthperimbutton;
    QPushButton *selectfilebutton;
    QPushButton *selectcolorbutton;
    QSpinBox *penWidthSpinBox;
    QComboBox *penStyleComboBox;
    QCheckBox *antialiasingCheckBox;
    QColor color;
    QStatusBar *statusBar;
    bool colorisselected;
public:
    Window(QWidget *parent = 0);
public slots:
    void setstatusbar(QString);
private slots:
    void penChanged();
    void selectcolor();    
};

#endif // WINDOW_H
