#include "pdir.h"
#include <QWidget>
void Pdir::setpd(QPointF point1,int dir1,int dir2)
{
    point=point1;
    direction1=dir1;
    direction2=dir2;
};
QPointF Pdir::getp()
{
    return point;
};
int Pdir::getdir1()
{
    return direction1;
};
int Pdir::getdir2()
{
    return direction2;
};
void Pdir::remove()
{
    direction1=0;
    direction2=0;
    point.setX(0);
    point.setY(0);
}
