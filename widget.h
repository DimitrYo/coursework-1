#ifndef WIDGET_H
#define WIDGET_H
#include <QPen>
#include <QWidget>
#include <QFile>
#include <pdir.h>
class Pdir;
class Widget : public QWidget
{
    Q_OBJECT
    QPen pen;
    bool antialiased;
    bool paint;
    bool paintbeginrects;
    bool paintendrects;
    bool paintpoly;
    bool fileisread;
    bool lenperiscliked;
    bool squareissearch;
    bool fileisselected;
    bool colorisselected;
    int N;
    int N1;
    int K;
    QFile *file;
    QPainter *painter;
    QVector<QRectF> *vectbegin,*vectend;
    QVector<Pdir> *pointd;
    QPolygonF *poly,*poly1;
    QString *filename;
    void paintEvent(QPaintEvent *event);
    void readfromfile();
    void lenghtperimetr();
    void lenghtperimetr1(QPolygonF *polyn);
    int  lenghtsearch(QPolygonF *poly2);
    void square();
    void square1();
    void square2();
    void square3();
    void squarevalue();
public:
    Widget(QWidget *parent = 0);
public slots:
    void setPen(const QPen &pen);
    void setAntialiased(bool antialiased);
    void setpaint();
    void cleanscreen();
    void setpaintpoly();
    void setpaintbegin();
    void selectfile();
    void setpaintend();
signals:
    void lenghtChanged(int lenght);
    void perimetrChanged(int lenght);
    void squareChanged(int lenght);
    void statusbarmassega(QString);
};
#endif // WIDGET_H
