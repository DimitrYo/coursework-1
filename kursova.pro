#-------------------------------------------------
#
# Project created by QtCreator 2011-05-11T17:09:43
#
#-------------------------------------------------

QT       += core \
             gui

TARGET = kursova
TEMPLATE = app


SOURCES += main.cpp \
    window.cpp \
    widget.cpp \
    pdir.cpp

HEADERS  += \
    window.h \
    widget.h \
    pdir.h
