#include <QtGui>
#include "widget.h"
#include <QVector>
//������� ��� ��������� ��������������� � ����� ��������� �� ������ PDir
bool qGreated(QRectF &rect1,QRectF &rect2);
bool qGreated1(Pdir &elem1 ,Pdir &elem2);

Widget::Widget(QWidget *)
{
    setFixedSize(500, 500);
    K=0;
    antialiased = false;//��������� �����������
    paint=false;// ������ �� ������
    paintbeginrects=false;//�� ������ ��������� ���������
    fileisread=false;//���� �� ��������
    lenperiscliked=false;//����� �� ���������
    fileisselected=false;//���� �� ������
    colorisselected=false;//���� �� ������ �� ��������� �����
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);//����������� ����
    vectbegin = new QVector<QRectF>;//�������� ������ ��� ��������� ��
    vectend = new QVector<QRectF>;// ������ ���������������
    pointd = new QVector<Pdir>;//���������� ������ ��� ����� ���������
    poly = new QPolygonF;// PDir � ����� ����� �������� � ����������� �������
    poly1 =new QPolygonF;
    filename = new QString;
};

void Widget::square()
{
    int i;
    N1=N;
    vectend->clear();//������� �����
    for(i=0;i<N1;i++)//�������� ��������� ������ � ����� �����
    vectend->append(vectbegin->at(i));
    square1();//�������� ���������
    square2();
    square3();
    squarevalue();
};
//�������� ������� ������
void Widget::squarevalue()
{
    int squareval,i;
    QRectF r1;//�������� ���������������
    for(i=0;i<N1;i++)
    {
     r1=vectend->at(i);//����� i ������������� �� ���������� ������� �������
     squareval+=(int)((((int)r1.height())*((int)r1.width())));
    }
    emit squareChanged(squareval);//�������� ������, ��� ���������� �������
    emit statusbarmassega("Square found successful");// �������� ������ ����� � ���������� ��������� ���������
};
//�������� ������
void Widget::square1()//������ 1.2, 1.3, 2.2, 2.3 �� � ������������
{
    int i,j;
    QRectF r1,r2,r3,r;
    for(i=0;i<N1;i++)
   { r1=vectend->at(i);
      for(j=i+1;j<N1;j++)
   {
    r2=vectend->at(j);
    if (r1.right()<r2.left()) break;//���� �������������� �� �������������
        else  //���� �� ��������� r1 �������������
    if (r1.intersects(r2)) //����� ��������� ������������� �� ��������������
        {
        if (r1.contains(r2))//���� ������ ���������� � ������
        {
            vectend->remove(j);//������� ������ � ������� �� �����
            --N1;//�������� �������� ���������� ��������� �� 1
            break;
        }
        if (r2.contains(r1))//���������� � �������
        {
            vectend->remove(i);
            --N1;
            break;
        }
        r3=r1&r2;//������� ������������ �����������
            if  (!r3.isNull())//���� �� �� �������
            {
            if (r3.top()==r1.top()) //1 (1-4)���� ���������� ������� ������ �����
             {
               if ((r3.bottom())<(r1.bottom())) //1.1 ,1.2
                { if ((r3.right())<(r1.right()))//1.2
                    {
                    r2.setBottomRight(r3.topRight());//�������� ������ ������ ����� r1 �� ������� ������ r3
                    vectend->replace(j,r2);//�������������� � ������� �������������
                    }
                } else
                   if ((r3.right())==(r1.right()))//1.3 ���� ���������� ������ ������ �����
                  {
                    r1.setRight(r3.left());//�������� ������ ������� ������� ��������������
                    vectend->replace(i,r1);
                  }
            }

            else //2
                    if ((r3.bottom())==(r1.bottom()))//2.1,2.2
                     {
                       if ((r3.right())<(r1.right()))//2.2
                       {
                        r2.setTop(r3.bottom());
                        vectend->replace(j,r2);
                       }
                     }
                     else //2.3
                     {
                     r2.setLeft(r3.right());
                     vectend->replace(j,r2);
                     }

            }
        }
    }
   }
};
//�������� ������
void Widget::square2()//���������� ���������� ������ ���������� 3 ������
{
    int i,j;
    QRectF r1,r2,r3,r;
    for(i=0;i<N1;i++)
   {
      r1=vectend->at(i);
      for(j=i+1;j<N1;j++)
        {
        r2=vectend->at(j);
        if (r1.right()<r2.left()) break;
            else
            if (r1.intersects(r2))
            {
            if (r1.contains(r2))
            {
                vectend->remove(j);
                --N1;
                break;
            }
            if (r2.contains(r1))
            {
                vectend->remove(i);
                --N1;
                break;
            }
            r3=r1&r2;
                if  (!r3.isNull())
                {
                 if (r3.top()==r1.top()) //1 (1-4)
                 {
                   if ((r3.bottom())<(r1.bottom())) //1.1 ,1.2
                    {
                        { //1.1
                          if (r1.left()==r3.left())
                          {
                              r1.setTop(r3.bottom());
                              vectend->replace(i,r1);
                              break;
                          } else
                          {
                          r2.setLeft(r3.right());
                          vectend->replace(j,r2);
                          r2.setLeft(r3.left());
                          r2.setBottomRight(r3.topRight());
                          vectend->append(r2);// ��������� ������������� � �����
                          ++N1;
                          }
                        }

                    } else
                         {
                            r.setTopLeft(r3.bottomLeft());
                            r.setBottomRight(r2.bottomRight());
                            r2.setBottom(r3.top());
                            vectend->replace(j,r2);
                            vectend->append(r);
                            ++N1;
                         }
                    }

                else //2
                        if ((r3.bottom())==(r1.bottom()))//2.1,2.2
                         {
                           if ((r3.right())==(r1.right()))//2.2
                           {
                               r2.setLeft(r3.right());
                               vectend->replace(j,r2);
                               r2.setLeft(r3.left());
                               r2.setTopRight(QPoint(r3.right(),r3.bottom()));
                               vectend->append(r2);
                               ++N1;
                           }
                         }

                }
            }
        }
   }
};
//������ ������������
void Widget::square3()
{
    qSort(vectend->begin(),vectend->end(),qGreated);//���������� �� �������������� ���������������
    qint8 i,j;
    QRectF r1,r2;
    for(i=0;i<N1;i++)
   {
      r1=vectend->at(i);
      for(j=i+1;j<N1;j++)//������� ����������� �� ������ ����
        {
        r2=vectend->at(j);
        if (r1.left()<r2.left()) break;
        if (r1.left()==r2.left())
        {
            if (r1.top()==r2.bottom())
            {
                if (r1.right()!=r2.right())
                {
                if (r1.right()<r2.right())
                {
                r1.setTop(r2.top());
                r2.setLeft(r1.right());
                vectend->replace(j,r2);
                vectend->replace(i,r1);
                }
                else
                {
                 r1.setLeft(r2.right());
                 r2.setBottom(r1.bottom());
                 vectend->replace(j,r2);
                 vectend->replace(i,r1);
                }
                }
                else
                {
                r1.setTop(r2.top());
                vectend->replace(i,r1);
                vectend->remove(j);
                --N1;
                }

                } else

            if (r2.top()==r1.bottom())
            {
                if (r1.right()!=r2.right())
                {
                if (r1.right()<r2.right())
                {
                r1.setBottom(r2.bottom());
                r2.setLeft(r1.right());
                vectend->replace(j,r2);
                vectend->replace(i,r1);
                }
                else
                {
                r2.setTop(r1.top());
                r1.setLeft(r2.right());
                vectend->replace(j,r2);
                vectend->replace(i,r1);
                }
                }
                else
                {
                r1.setBottom(r2.bottom());
                vectend->replace(i,r1);
                vectend->remove(j);
                --N1;
                }
            }
        }
        }
   }

    for(i=0;i<N1;i++)//������� ���������� �� ������� ����
   {
      r1=vectend->at(i);
      for(j=i+1;j<N1;j++)
        {
        r2=vectend->at(j);
        if (r1.right()==r2.right())
        {
            if (r1.top()==r2.bottom())
            {
                if (r1.left()!=r2.left())
                {
                if (r1.left()>r2.left())
                {
                r1.setTop(r2.top());
                r2.setRight(r1.left());
                vectend->replace(j,r2);
                vectend->replace(i,r1);
                }
                else
                {
                 r1.setRight(r2.left());
                 r2.setBottom(r1.bottom());
                 vectend->replace(j,r2);
                 vectend->replace(i,r1);
                }
                }
                else
                {
                r1.setTop(r2.top());
                vectend->replace(i,r1);
                vectend->remove(j);
                --N1;
                }
                } else

            if (r2.top()==r1.bottom())
            {
                if (r1.left()!=r2.left())
                {
                if (r1.left()>r2.left())
                {
                r1.setBottom(r2.bottom());
                r2.setRight(r1.left());
                vectend->replace(j,r2);
                vectend->replace(i,r1);
                }
                else
                {
                r2.setTop(r1.top());
                r1.setRight(r2.left());
                vectend->replace(j,r2);
                vectend->replace(i,r1);
                }
                }
                else
                {
                r1.setBottom(r2.bottom());
                vectend->replace(i,r1);
                vectend->remove(j);
                --N1;
                }
            }
            }
        }
    }

};
//��������� �����
void Widget::lenghtperimetr()
{

    poly->clear();
    poly1->clear();
    Pdir elem,elem1;
    int i,j;
    QRectF r1,r2;
    QPointF p,p1;//���������� ����� � �������� �����������
    for(i=0;i<N;i++)// ���� ���������� ����� � ��������� ����� ��������� PDIR
    {
        elem.remove();//������ ��������
        r1=vectbegin->at(i);
        p.setX(r1.left());//������ ����� p ���������� � ������ ������ ���� ��������������
        p.setY(r1.bottom());//���������� �
        elem.setpd(p,4,1);//������ �������� �������� ����� � ����������� 4 � 1 -����� ������ �����
        pointd->append(elem);
        elem.remove();
        p.setY(r1.top());
        elem.setpd(p,1,2);
        pointd->append(elem);
        elem.remove();
        p.setX(r1.right());
        elem.setpd(p,2,3);
        pointd->append(elem);
        p.setY(r1.bottom());
        elem.setpd(p,3,4);
        pointd->append(elem);
        elem.remove();
        K=K+4;//�������� 4 �����
        j=i-1;
       while(j>=0)// ��������� ����� ����������� � ����������� ���������������� �� �������
       {
           r2=vectbegin->at(j);
           QRectF r3;
           r3=r1&r2;


               if  (!r3.isNull())
                {
                 if (r3.top()==r2.top()) //1 (1-4)
                 {
                   if ((r3.bottom())<(r2.bottom())) //1.1 ,1.2
                    { if ((r3.right())<(r2.right()))//1.2
                        {
                         p.setY(r3.top());
                         p.setX(r3.left());
                         elem.setpd(p,2,1);
                         pointd->append(elem);
                         p.setX(r3.right());
                         elem.setpd(p,3,2);
                         pointd->append(elem);
                         elem.remove();
                         K=K+2;
                        } else
                        { //1.1
                         p.setY(r3.top());
                         p.setX(r3.left());
                         elem.setpd(p,2,1);
                         pointd->append(elem);
                         p.setX(r3.right());
                         p.setY(r3.bottom());
                         elem.setpd(p,4,3);
                         pointd->append(elem);
                         elem.remove();
                         K=K+2;
                        }

                    } else
                       if ((r3.right())==(r2.right()))//1.3
                      {
                       p.setY(r3.top());
                       p.setX(r3.left());
                       elem.setpd(p,2,1);
                       pointd->append(elem);
                       p.setY(r3.bottom());
                       elem.setpd(p,1,4);
                       pointd->append(elem);
                       elem.remove();
                       K=K+2;
                      }
                       else //1.4
                         {   p.setX(r3.left());
                             p.setY(r3.bottom());
                             elem.setpd(p,1,4);
                             pointd->append(elem);
                             elem.remove();
                             p.setY(r3.top());
                             elem.setpd(p,2,1);
                             pointd->append(elem);
                             elem.remove();
                             p.setX(r3.right());
                             elem.setpd(p,3,2);
                             pointd->append(elem);
                             p.setY(r3.bottom());
                             elem.setpd(p,4,3);
                             pointd->append(elem);
                             elem.remove();
                             K=K+4;
                        }
                    }

                else //2
                        if ((r3.bottom())==(r2.bottom()))//2.1,2.2
                         {
                           if ((r3.right())<(r2.right()))//2.2
                           {
                            p.setY(r3.bottom());
                            p.setX(r3.left());
                            elem.setpd(p,1,4);
                            pointd->append(elem);
                            p.setX(r3.right());
                            elem.setpd(p,4,3);
                            pointd->append(elem);
                            elem.remove();
                            K=K+2;
                           } else//2.1
                           {
                            p.setY(r3.top());
                            p.setX(r3.right());
                            elem.setpd(p,3,2);
                            pointd->append(elem);
                            p.setX(r3.left());
                            p.setY(r3.bottom());
                            elem.setpd(p,1,4);
                            pointd->append(elem);
                            elem.remove();
                            K=K+2;
                           }
                        } else //2.3
                        {
                         p.setY(r3.top());
                         p.setX(r3.right());
                         elem.setpd(p,3,2);
                         pointd->append(elem);
                         p.setY(r3.bottom());
                         elem.setpd(p,4,3);
                         pointd->append(elem);
                         elem.remove();
                         K=K+2;
                        }
                }
          --j;
        }
    }
    for(i=0;i<K;i++)//�������� ���������� ����� �������� ������
    {//����� ����������� � �������� �������
        elem=pointd->at(i);
        for(j=0;j<N;j++)
        {
                r1=vectbegin->at(j);
            if ((elem.getp().rx()>r1.left())//��������� ������� �� �����
                    &&(elem.getp().rx()<r1.right())
                    &&(elem.getp().ry()>r1.top())
                    &&(elem.getp().ry()<r1.bottom()))
            {
                pointd->remove(i);//������� �����
                --K;
                --i;
                break;
            }
        }
    }
    qSort(pointd->begin(),pointd->end(),qGreated1);//��������� ����� �� ����������� ���������� �
    lenghtperimetr1(poly);//�������� ������ ������� ������� ������� ������
    int lenght,perimetr;
    lenght=0;
    lenght=lenghtsearch(poly);//������� ����� �������� �������
    emit lenghtChanged(lenght);//������� ��������� � ������ �������� �������
    if (K>0)
    lenghtperimetr1(poly1);//������� ���������� ���������� ������
    perimetr=0;
    perimetr=lenght+lenghtsearch(poly1);//������� ��������
    emit perimetrChanged(perimetr);
};
//����������� ����������� �������
void Widget::lenghtperimetr1(QPolygonF *polyn)
{
    Pdir elem,elem1;
    int i,j,m;
    QRectF r1,r2;
    QPointF p,p1;
    bool addpoint;//��������� �� �����?
    i=0;
    elem=pointd->at(i);//����� ����� ����� ����� ������� ����� ��������� ������
    j=elem.getdir2();//����������� ������ �����������
    p=elem.getp();//�������� �����
    polyn->append(p);
    while((p!=p1)&&(K>0))//���� �� ���������� � ������ ������ ��� ���� ����� �� ���������
    {
        addpoint=false;//����� �� ���������
        // ��������� �����������
        if (j==1)
            {
                if (!addpoint)
                for(m=i;m<K;m++)//��������� � ����� ������ � ����� ��������� �����
                {
                    elem1=pointd->at(m);
                    if ((j==elem1.getdir1())&&((elem.getp().x())==(elem1.getp().x()))&&
                            ((elem.getp().y())>(elem1.getp().y())))
                    {
                        p1=elem1.getp(); polyn->append(p1);
                        addpoint=true;
                        elem=pointd->at(m);
                        j=elem1.getdir2();
                        pointd->remove(m);
                        --K;
                        if (m==K) --m;
                        i=m;
                        break;
                    }
                }
                if (!addpoint)
                    for(m=i;m>=0;m--)//��������� � ������ ������ � ����� ��������� �����
                    {
                        elem1=pointd->at(m);
                        if ((j==elem1.getdir1())&&((elem.getp().x())==(elem1.getp().x()))&&
                                ((elem.getp().y())>(elem1.getp().y())))
                        {
                            p1=elem1.getp(); polyn->append(p1);//��������� ����� �� �������
                            addpoint=true;
                            elem=pointd->at(m);
                            j=elem1.getdir2();
                            pointd->remove(m);
                            --K;
                            if (m==K) --m;
                            i=m;
                            break;
                        }
                    }

        }else//��� ��������� ����������
        if (j==2)
        {
            if (!addpoint)
            for(m=i;m<K;m++)
            {

                elem1=pointd->at(m);
                if ((j==elem1.getdir1())&&((elem.getp().y())==(elem1.getp().y()))&&
                        ((elem.getp().x())<(elem1.getp().x())))
                {
                    p1=elem1.getp(); polyn->append(p1);
                    addpoint=true;
                    elem=pointd->at(m);
                    j=elem1.getdir2();
                    pointd->remove(m);
                    --K;
                    if (m==K) --m;
                    i=m;
                    break;
                }
            }
        }else
        if (j==3)
        {
            if (!addpoint)
            for(m=i;m<K;m++)
            {
                elem1=pointd->at(m);
                if ((j==elem1.getdir1())&&((elem.getp().x())==(elem1.getp().x()))&&
                        ((elem.getp().y())<(elem1.getp().y())))
                {
                    p1=elem1.getp(); polyn->append(p1);
                    addpoint=true;
                    elem=pointd->at(m);
                    j=elem1.getdir2();
                    pointd->remove(m);
                    --K;
                    if (m==K) --m;
                    i=m;
                    break;
                }
            }
            if (!addpoint)
            {
                for(m=i;m>=0;m--)
                {
                    elem1=pointd->at(m);
                    if ((j==elem1.getdir1())&&((elem.getp().x())==(elem1.getp().x()))&&
                            ((elem.getp().y())<(elem1.getp().y())))
                    {
                        p1=elem1.getp(); polyn->append(p1);
                        addpoint=true;
                        elem=pointd->at(m);
                        j=elem1.getdir2();
                        pointd->remove(m);
                        --K;
                        if (m==K) --m;
                        i=m;
                        break;
                    }
                }
            }
        }else
        if (j==4)
        {
            if (!addpoint)
            for(m=i;m<K;m++)
            {
                elem1=pointd->at(m);
                if ((j==elem1.getdir1())&&((elem.getp().y())==(elem1.getp().y()))&&
                        ((elem.getp().x())>(elem1.getp().x())))
                {
                    p1=elem1.getp(); polyn->append(p1);
                    addpoint=true;
                    elem=pointd->at(m);
                    j=elem1.getdir2();
                    pointd->remove(m);
                    --K;
                    if (m==K) --m;
                    i=m;
                    break;
                }
            }
            if (!addpoint)
            {
                for(m=i;m>=0;m--)
                {
                    elem1=pointd->at(m);
                    if ((j==elem1.getdir1())&&((elem.getp().y())==(elem1.getp().y()))&&
                            ((elem.getp().x())>(elem1.getp().x())))
                    {
                        p1=elem1.getp(); polyn->append(p1);
                        addpoint=true;
                        elem=pointd->at(m);
                        j=elem1.getdir2();
                        pointd->remove(m);
                        --K;
                        if (m==K) --m;
                        i=m;
                        break;
                    }
                }
            }
        }

    }
};
//���������� ������� �������
int Widget::lenghtsearch(QPolygonF *poly2)
{
    int x,i,j;
    x=0;
    QPointF p,p1;
    i=poly2->size();//���������� ���������� ���������
    for(j=0;j<i-1;j++)//�� ����������
    {
        p=poly2->at(j);
        p1=poly2->at(j+1);
        if (p.x()==p1.x())//������� ��������� ����� �������
            x+=(int)qAbs(p1.y()-p.y());//�� ������ ������ �� ������)
        if (p.y()==p1.y())
            x+=(int)qAbs(p1.x()-p.x());
    }
    emit statusbarmassega("Length found successful");//������ ��������� � ������ ����
    return x;
};
//��������� �� �������
void Widget::paintEvent(QPaintEvent * )
{
    painter = new QPainter(this);//�������� ������ ��� ������������
    if (paint)//���� ����� ��������
    {
    painter->begin(this);//�������� ���������
    painter->setPen(pen);//�������� �����
    if (antialiased)//��������/��������� �����������
       painter->setRenderHint(QPainter::Antialiasing, true);
        if (paintbeginrects)//������ ��������� ��������
        {
        painter->drawRects(*vectbegin);
        }
        if (paintpoly)//������ �������
        {
         if (!(poly->isEmpty()))
         painter->drawPolygon(*poly);
         if (!(poly1->isEmpty()))
         painter->drawPolygon(*poly1);
        }
        if (paintendrects)//������ ���������� ����� ��������� ��������
        {
        painter->drawRects(*vectend);
        }
    painter->setRenderHint(QPainter::Antialiasing, false);
    painter->setPen(palette().dark().color());
    painter->end();
    }
    delete painter;
};
//���� �����
void Widget::selectfile()
{
    QFileDialog dialog(this);//�������� ���������� ���� ������ �����
        dialog.setFileMode(QFileDialog::AnyFile);//����� �����
        dialog.setViewMode(QFileDialog::Detail);//��������� ����������
        *filename = dialog.getOpenFileName(this,//���������� ������ � ������ ��������� �����
             tr("Open Text")," ", tr("Text Files ( *.txt);; All Files *.*()"));
        fileisselected=true;//���� ������
        if (filename->isEmpty())//���� ��� ����� ������
        fileisselected=false;
       if (fileisselected)
       {
        fileisread=true;
        lenperiscliked=false;
        squareissearch=false;
        paint=true;
        N=0;
        K=0;
        N1=0;
        readfromfile();//��������� �� �����
        emit lenghtChanged(0);//������������ ���������� �� 0
        emit perimetrChanged(0);
        emit squareChanged(0);
        if (!paintbeginrects)
        {
        paintbeginrects=true;//���������� ��������� ������
        paintpoly=false;
        paintendrects=false;
        }
        update();//���������� ����������� ������ toLetin1()
        emit statusbarmassega(tr("File "+filename->toLatin1()+" is successful selected"));
        } else
        emit statusbarmassega("File is not selected");
};
//���������� � �����
void Widget::readfromfile()
{
    int n1,n2,n3,n4,j;
    file = new QFile(*filename);
     if (!file->open(QIODevice::ReadWrite))//��������� ���� ��� ������
     {
     emit statusbarmassega("File Error");
     }
    QTextStream in(file);//�������� ��������� �����
    QString string;
    QStringList numbers;
    string=in.readLine();//��������� ������ ������ � ����������� ���������
    if (!string.toInt())//���� ��������� ������ ��� �����������
    {
        fileisread=false;
        emit statusbarmassega("File is BAD. Select another file!!!");
    } else
    {
    N=string.toInt();
    vectbegin->clear();
    for(j=0;j<N;j++)
    {
        QRectF rect(0,0,0,0);
        string= in.readLine();
        //string.capacity();
        string.simplified();//������� ������ ����������� �����(���������)
        numbers= string.split(",");//��������� ������ ��������� � ������ ������� ���� ��������� ��������
        if(numbers.at(0).toInt()&&numbers.at(1).toInt()&&
                numbers.at(2).toInt()&&numbers.at(3).toInt())//���� ����������� ������ �������
        {//����� ���� ������
        n1=numbers.at(0).toInt();
        n2=numbers.at(1).toInt();
        n3=numbers.at(2).toInt();
        n4=numbers.at(3).toInt();
        rect.adjust(qMin(n1,n3),//��������� ���������� � ��������
                    qMin(n2,n4),
                    qMax(n1,n3),
                    qMax(n2,n4));
        vectbegin->append(rect);
        } else
        {
            fileisread=false;
            emit statusbarmassega("File is BAD. Select another file!!!");
        }
    }
    qSort(vectbegin->begin(),vectbegin->end(),qGreated);
    }
};
//������� ��� ��������� ����� �� ������������
bool qGreated(QRectF &rect1 ,QRectF &rect2)
{
    return (rect2.left() - rect1.left())>0;//���������� ������ ���� ����� ���� ������� �����
};

bool qGreated1(Pdir &elem1 ,Pdir &elem2)
{
   if (elem1.getp().x()<elem2.getp().x()) return true;
   if (elem1.getp().x()>elem2.getp().x()) return false;
   if (elem1.getp().y()<elem2.getp().y()) return true;
   if (elem1.getp().y()>elem2.getp().y()) return false;
   return false;
};
//������� ������� ������
void Widget::setpaintend()
{//���� ���� �������� ������������� ��������� �������� ���������
  if (fileisread)
  {
      if (!squareissearch)
      {
      squareissearch=true;
      square();
      }
      if (!paintendrects)
      {
      paintbeginrects=false;
      paintendrects=true;
      paintpoly=false;
      }
      update();
  }
};

void Widget::setPen(const QPen &pen)
{
    this->pen = pen;
    update();
};

void Widget::setAntialiased(bool antialiased)
{
    this->antialiased = antialiased;
    update();
};

void Widget::setpaint()

{
    if (fileisselected)
    {
    this->paint=true;
    update();
    }
};
//������� �����
void Widget::cleanscreen()
{
    paint=false;
    update();
    emit statusbarmassega("Screen is cleaned");
};
//������� �������� ��������
void Widget::setpaintbegin()
{
    if (!fileisselected)
    {
        emit statusbarmassega("File Error. Please select file");
    } else
    {
        fileisread=true;
        readfromfile();
    }
    if (!paintbeginrects)
    {
    paintbeginrects=true;
    paintendrects=false;
    paintpoly=false;
    }
    update();
};
//������� ��������� ������
void Widget::setpaintpoly()
{   if (fileisread)
    {
        if (!lenperiscliked)
    {
        lenperiscliked=true;
        lenghtperimetr();
    }
    if (!paintpoly)
    {
    paintpoly=true;
    paintbeginrects=false;
    paintendrects=false;
    }
    update();
    }
};
