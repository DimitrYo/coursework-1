#include <QtGui>
#include <QWidget>
#include "window.h"
#include "widget.h"
const int IdRole = Qt::UserRole;
//ewgwegwgwe
Window::Window(QWidget *)
{
    setFixedSize(750, 550);
    widget = new Widget;
    //����� ������� �����
    penWidthSpinBox = new QSpinBox;
    penWidthSpinBox->setRange(0, 3);
    penWidthSpinBox->setSpecialValueText(tr("0 (cosmetic pen)"));
    //������� ������� �����
    penWidthLabel = new QLabel(tr("Pen &Width:"));
    penWidthLabel->setBuddy(penWidthSpinBox);
    //������� ������� ��� ������ ���� �����
    penStyleComboBox = new QComboBox;
    penStyleComboBox->addItem(tr("Solid"), Qt::SolidLine);
    penStyleComboBox->addItem(tr("Dash"), Qt::DashLine);
    penStyleComboBox->addItem(tr("Dot"), Qt::DotLine);
    penStyleComboBox->addItem(tr("Dash Dot"), Qt::DashDotLine);
    penStyleComboBox->addItem(tr("Dash Dot Dot"), Qt::DashDotDotLine);
    penStyleComboBox->addItem(tr("None"), Qt::NoPen);
    //������� ����� �����
    penStyleLabel = new QLabel(tr("&Pen Style:"));
    penStyleLabel->setBuddy(penStyleComboBox);
    //������� ������
    startbutton = new QPushButton(tr("&Begin"),this);
    squarebutton = new QPushButton(tr("&Square"),this);
    clearscreenbutton = new QPushButton(tr("&Cleanscreen"),this);
    quitbutton = new QPushButton(tr("&Quit"),this);
    lengthperimbutton = new QPushButton(tr("Search &Lenght"),this);
    selectfilebutton = new QPushButton(tr("Select &File"),this);
    selectcolorbutton = new QPushButton(tr("Select Color"),this);

    squareLCD = new QLCDNumber(7); //���������� �����
    squareLCD->setSegmentStyle(QLCDNumber::Filled);
    squareLabel = new QLabel(tr("Square"));
    squareLabel->setBuddy(squareLCD);
    lengthLCD = new QLCDNumber(7);
    lengthLCD->setSegmentStyle(QLCDNumber::Filled);
    lengthLabel = new QLabel(tr("Length"));
    lengthLabel->setBuddy(lengthLCD);
    perimetrLCD = new QLCDNumber(7);
    perimetrLCD->setSegmentStyle(QLCDNumber::Filled);
    perimetrLabel = new QLabel(tr("Perimetr"));
    perimetrLabel->setBuddy(perimetrLCD);

    statusBar = new QStatusBar;
    // ������ �����������
    antialiasingCheckBox = new QCheckBox(tr("&Antialiasing"));
    // ���������� �������� � ������
    connect(penWidthSpinBox, SIGNAL(valueChanged(int)),
            this, SLOT(penChanged()));
    connect(penStyleComboBox, SIGNAL(activated(int)),
            this, SLOT(penChanged()));
    connect(antialiasingCheckBox, SIGNAL(toggled(bool)),
            widget, SLOT(setAntialiased(bool)));
    connect(startbutton,SIGNAL(clicked()),
           widget,SLOT(setpaint()));
    connect(startbutton,SIGNAL(clicked()),
           widget,SLOT(setpaintbegin()));
    connect(lengthperimbutton,SIGNAL(clicked()),
           widget,SLOT(setpaint()));
    connect(lengthperimbutton,SIGNAL(clicked()),
           widget,SLOT(setpaintpoly()));
    connect(squarebutton,SIGNAL(clicked()),
           widget,SLOT(setpaint()));
    connect(squarebutton,SIGNAL(clicked()),
           widget,SLOT(setpaintend()));
    connect(selectfilebutton,SIGNAL(clicked()),
           widget,SLOT(selectfile()));
    connect(selectcolorbutton,SIGNAL(clicked()),
           this,SLOT(selectcolor()));
    connect(clearscreenbutton,SIGNAL(clicked()),
        widget,SLOT(cleanscreen()));
    connect(widget,SIGNAL(lenghtChanged(int)),
        lengthLCD,SLOT(display(int)));
    connect(widget,SIGNAL(perimetrChanged(int)),
        perimetrLCD,SLOT(display(int)));
    connect(widget,SIGNAL(squareChanged(int)),
        squareLCD,SLOT(display(int)));
    connect(quitbutton,SIGNAL(clicked()),SLOT(close()));
    connect(widget,SIGNAL(statusbarmassega(QString)),this,SLOT(setstatusbar(QString)));
    //��������� ����������
    QHBoxLayout *firstLayout= new QHBoxLayout;
    firstLayout->addWidget(penWidthLabel);
    firstLayout->addWidget(penWidthSpinBox);
    QHBoxLayout *secondLayout= new QHBoxLayout;
    secondLayout->addWidget(penStyleLabel);
    secondLayout->addWidget(penStyleComboBox);
    QHBoxLayout *thirdLayout= new QHBoxLayout;
    thirdLayout->addWidget(squareLabel);
    thirdLayout->addWidget(squareLCD);
    QHBoxLayout *fourthLayout= new QHBoxLayout;
    fourthLayout->addWidget(lengthLabel);
    fourthLayout->addWidget(lengthLCD);
    QHBoxLayout *fifthLayout= new QHBoxLayout;
    fifthLayout->addWidget(perimetrLabel);
    fifthLayout->addWidget(perimetrLCD);
    QVBoxLayout *leftLayout= new QVBoxLayout;
    leftLayout->addWidget(startbutton);
    leftLayout->addWidget(lengthperimbutton);
    leftLayout->addWidget(squarebutton);
    leftLayout->addWidget(clearscreenbutton);
    leftLayout->addLayout(fourthLayout);
    leftLayout->addLayout(fifthLayout);
    leftLayout->addLayout(thirdLayout);
    leftLayout->addLayout(firstLayout);
    leftLayout->addLayout(secondLayout);
    leftLayout->addWidget(antialiasingCheckBox,Qt::AlignHCenter);
    leftLayout->addWidget(selectfilebutton);
    leftLayout->addWidget(selectcolorbutton);
    leftLayout->addWidget(quitbutton);
    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addLayout(leftLayout);
    mainLayout->addWidget(widget);
    QVBoxLayout *Layout = new QVBoxLayout;
    Layout->addLayout(mainLayout);
    Layout->addWidget(statusBar,Qt::AlignHCenter);
    setLayout(Layout);

    statusBar->setSizeGripEnabled(false);
    statusBar->showMessage(tr("To start press SELECT FILE"),10000);
    color.setNamedColor("blue");
    colorisselected=false;
    penChanged();//������ ����� �����
    antialiasingCheckBox->setChecked(true);
    //�������� ��������� ����
    setWindowTitle(tr("Coursework. Yovchev D.K. DA-01"));
};
void Window::penChanged()
{
    int width = penWidthSpinBox->value();
    Qt::PenStyle style = Qt::PenStyle(penStyleComboBox->itemData(
            penStyleComboBox->currentIndex(), IdRole).toInt());
    widget->setPen(QPen(color,width, style,Qt::RoundCap));
};
void Window::selectcolor()//����� �����, �������, ����� �����
{
    int width = penWidthSpinBox->value();
    Qt::PenStyle style = Qt::PenStyle(penStyleComboBox->itemData(
            penStyleComboBox->currentIndex(), IdRole).toInt());
    QColorDialog colordialog;
    color= colordialog.getColor();
    widget->setPen(QPen(color,width, style,Qt::RoundCap));
    statusBar->showMessage("Color is selected",3000);
};
//����� ��������� � ������ ���
void Window::setstatusbar(QString massage)
{
    statusBar->showMessage(massage,5000);
};
